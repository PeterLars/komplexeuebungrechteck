package test;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.BunteRechteckeController;
import model.Rechteck;
import view.Zeichenflaeche;

@SuppressWarnings("serial")
public class RechteckViewTest extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application. (HA6 von Tenbusch)
	 */
	public static void main(String[] args) {
		new RechteckViewTest().run();
	}

	protected void run() {
		while (true) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block (HA6 von Tenbusch)
				e.printStackTrace();
			}
			getContentPane().repaint();
		}

	}

	/**
	 * Create the frame. (HA6 von Tenbusch)
	 */
	public RechteckViewTest() {
		BunteRechteckeController brc = new BunteRechteckeController();
		brc.generiereZufallsRechtecke(25);
		setTitle("RechteckViewTest");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 1260, 1080);
		contentPane = new Zeichenflaeche(brc);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		this.setVisible(true);
	}

}
