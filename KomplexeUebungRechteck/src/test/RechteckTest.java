package test;

import controller.BunteRechteckeController;
import model.Rechteck;

public class RechteckTest {

	public static void main(String[] args) {

		// 5 Rechtecke mit dem parameterlosen Konstruktor (HA3)
		Rechteck rechteck0 = new Rechteck();
		rechteck0.setX(10);
		rechteck0.setY(10);
		rechteck0.setBreite(30);
		rechteck0.setHoehe(40);

		Rechteck rechteck1 = new Rechteck();
		rechteck1.setX(25);
		rechteck1.setY(25);
		rechteck1.setBreite(100);
		rechteck1.setHoehe(20);

		Rechteck rechteck2 = new Rechteck();
		rechteck2.setX(260);
		rechteck2.setY(10);
		rechteck2.setBreite(200);
		rechteck2.setHoehe(100);

		Rechteck rechteck3 = new Rechteck();
		rechteck3.setX(5);
		rechteck3.setY(500);
		rechteck3.setBreite(300);
		rechteck3.setHoehe(25);

		Rechteck rechteck4 = new Rechteck();
		rechteck4.setX(100);
		rechteck4.setY(100);
		rechteck4.setBreite(100);
		rechteck4.setHoehe(100);

		// 5 Rechtecke mit dem vollparametrisierten Konstuktor (HA3)
		Rechteck rechteck5 = new Rechteck(200, 200, 200, 200);
		Rechteck rechteck6 = new Rechteck(800, 400, 20, 20);
		Rechteck rechteck7 = new Rechteck(800, 450, 20, 20);
		Rechteck rechteck8 = new Rechteck(850, 400, 20, 20);
		Rechteck rechteck9 = new Rechteck(855, 455, 25, 25);

		// Test, ob die Ausgabe der toString() Methode der Klasse Rechteck mit der
		// Vorgabe übereinstimmt (HA4)
		if (rechteck0.toString().equals("Rechteck [x=10, y=10, breite=30, hoehe=40]")) {
			System.out
					.println("Die Ausgabe der toString() Methode der Klasse Rechteck stimmt mit der vorgabe überein!");
		} else {
			System.err.println(
					"Die Ausgabe der toString() Methode der Klasse Rechteck stimmt mit der vorgabe NICHT überein!");
		}

		// Erstellen eines BunteRechteckeControllers und übergabe der 10 Test Rechtecke
		// (HA5)
		BunteRechteckeController TestRechteckController = new BunteRechteckeController();
		TestRechteckController.add(rechteck0);
		TestRechteckController.add(rechteck1);
		TestRechteckController.add(rechteck2);
		TestRechteckController.add(rechteck3);
		TestRechteckController.add(rechteck4);
		TestRechteckController.add(rechteck5);
		TestRechteckController.add(rechteck6);
		TestRechteckController.add(rechteck7);
		TestRechteckController.add(rechteck8);
		TestRechteckController.add(rechteck9);

		// Test, ob die Ausgabe der toString() Methode der Klasse
		// BunteRechteckeController mit der Vorgabe übereinstimmt (HA5)
		if (TestRechteckController.toString().equals(
				"BunteRechteckeController [rechtecke=[Rechteck [x=10, y=10, breite=30, hoehe=40], Rechteck [x=25, y=25, breite=100, hoehe=20], Rechteck [x=260, y=10, breite=200, hoehe=100], Rechteck [x=5, y=500, breite=300, hoehe=25], Rechteck [x=100, y=100, breite=100, hoehe=100], Rechteck [x=200, y=200, breite=200, hoehe=200], Rechteck [x=800, y=400, breite=20, hoehe=20], Rechteck [x=800, y=450, breite=20, hoehe=20], Rechteck [x=850, y=400, breite=20, hoehe=20], Rechteck [x=855, y=455, breite=25, hoehe=25]]]")) {
			System.out.println(
					"Die Ausgabe der toString() Methode der Klasse BunteRechteckeController stimmt mit der vorgabe überein!");
		} else {
			System.err.println(
					"Die Ausgabe der toString() Methode der Klasse BunteRechteckeController stimmt mit der vorgabe NICHT überein!");
		}

		// Prüfen ob Rechteckerstellung mit negativen Breiten und Höhen möglich ist
		// (HA8)
		Rechteck eck10 = new Rechteck(-4, -5, -50, -200);
		System.out.println(eck10); // Rechteck [x=-4, y=-5, breite=50, hoehe=200] (HA8)
		Rechteck eck11 = new Rechteck();
		eck11.setX(-10);
		eck11.setY(-10);
		eck11.setBreite(-200);
		eck11.setHoehe(-100);
		System.out.println(eck11);// Rechteck [x=-10, y=-10, breite=200, hoehe=100] (HA8)

		// Test der generiereZufallsRechteck Methode (HA13)
		Rechteck eck12 = Rechteck.generiereZufallsRechteck();
		System.out.println("Zufallsrechteck: " + eck12);

		// Test der Zufallsgenerierung (HA14)
		if (rechteckeTesten()) {
			System.out.println("Die 50.000 Rechtecke befinden sich innerhalb des Limits!");
		} else {
			System.err.println("Mindestens eins der 50.000 Rechtecke befindet sich nicht im Limit!");
		}

		// Ende der Testklasse

	}

	// Eine Methode zum Testen der Zufallsgenerierung mit Hilfe von 50.000 Tests
	// (HA14)
	public static boolean rechteckeTesten() {
		Rechteck[] rechtecke = new Rechteck[50000];
		Rechteck grenze = new Rechteck(0, 0, 1200, 1000);
		for (int i = 0; i < rechtecke.length; i++)
			rechtecke[i] = Rechteck.generiereZufallsRechteck();
		for (int i = 0; i < rechtecke.length; i++)
			if (!grenze.enthaelt(rechtecke[i]))
				return false;
		return true;
	}

}
