package model;

public class Punkt {
	
	// Attribute (HA9)
	private int x;
	private int y;
	
	// Konstruktoren (HA9)
	public Punkt() {
		this.setX(0);
		this.setY(0);
	}
	
	public Punkt(int x, int y) {
		this.setX(x);
		this.setY(y);
	}
	
	// Gibt die das Rechteck mit seinen Daten in einem String aus (HA9)
	@Override
	public String toString() {
		return "Punkt [x=" + x + ", y=" + y + "]";
	}
	

	// Vergleicht zwei Punkte miteinander (HA9)
	public boolean equals(Punkt p) {
		if (this == p)
			return true;
		if (x != p.x)
			return false;
		if (y != p.y)
			return false;
		return true;
	}

	// Getter und setter (HA9)
	public int getX() {
		return x;
	}
	
	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
}
