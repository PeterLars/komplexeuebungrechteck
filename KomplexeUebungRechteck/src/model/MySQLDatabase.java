package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

public class MySQLDatabase {

	private final String driver = "com.mysql.jdbc.Driver";
	private final String url = "jdbc:mysql://localhost/rechtecke?";
	private final String user = "root";
	private final String password = "";

	public void rechteckEintragen(Rechteck r) {
		try {
			// JDBC-Treiber laden
			Class.forName(driver);
			// Verbindung aufbauen
			 Connection con = DriverManager.getConnection(url, user, password);
			// SQL Befehl
			String sql = "INSERT INTO T_Recktecke (x, y, breite, hoehe) VALUES" + 
					"(?, ?, ?, ?);";
			// Statement erstellen
			PreparedStatement ps = con.prepareStatement(sql);
			// Werte eintragen
			ps.setInt(1, r.getX());
			ps.setInt(2, r.getY());
			ps.setInt(3, r.getBreite());
			ps.setInt(4, r.getHoehe());
			// Statement ausführen
			ps.executeUpdate();
			// Verbindung schließen
			con.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public List<Rechteck> getAlleRechtecke() {
		List<Rechteck> rechtecke = new LinkedList<Rechteck>();
		try {
			// JDBC-Treiber laden
			Class.forName(driver);
			// Verbindung aufbauen
			 Connection con = DriverManager.getConnection(url, user, password);
			// SQL Befehl
			String sql = "SELECT * FROM T_Recktecke;";
			// Statement erstellen
			Statement stmt = con.createStatement();
			// Statement ausführen
			ResultSet rs = stmt.executeQuery(sql);
			// Liste auswerten
			while (rs.next()) {
				int x = rs.getInt("x"); 
				int y = rs.getInt("y");
				int breite = rs.getInt("breite"); 
				int hoehe = rs.getInt("hoehe");
				rechtecke.add(new Rechteck(x ,y ,breite ,hoehe));
			}
			// Verbindung schließen
			con.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return rechtecke;
	}
	
}
