package model;

public class Rechteck {

	// Attribute (HA2)
	private int breite;
	private int hoehe;
	//X und Y zu Punkt vereint (HA10)
	private Punkt punkt;

	// Konstruktoren (HA2)
	public Rechteck() {
		super();
		this.punkt = new Punkt(0,0);
		setBreite(0);
		setHoehe(0);
	}

	public Rechteck(int x, int y, int breite, int hoehe) {
		super();
		this.punkt = new Punkt(x,y);
		setBreite(breite);
		setHoehe(hoehe);
	}

	// Gibt die das Rechteck mit seinen Daten in einem String aus (HA4)
	@Override
	public String toString() {
		return "Rechteck [x=" + this.getX() + ", y=" + this.getY() + ", breite=" + this.getBreite() + ", hoehe=" + this.getHoehe() + "]";
	}

	// Methode die schaut ob eine bestimmte Koordinate im Rechteck liegt (HA11)
	public boolean enthaelt(int x, int y) {
		if (this.getX() <= x && this.getX() + this.getBreite() >= x && this.getY() <= y
				&& this.getY() + this.getHoehe() >= y) {
			return true;
		}
		return false;
	}

	// Methode die schaut ob ein bestimmter Punkt im Rechteck liegt (HA11)
	public boolean enthaelt(Punkt p) {
		return enthaelt(p.getX(), p.getY());
	}

	// Methode die schaut ob ein bestimmtes Rechteck in diesem Rechteck liegt (HA12)
	public boolean enthaelt(Rechteck rechteck) {
		return enthaelt(rechteck.getX(), rechteck.getY()) && enthaelt(rechteck.getX()+rechteck.breite, rechteck.getY()+ rechteck.hoehe);
	}

	//Eine Methode, die ein zufälliges Rechteck im bereich von 1200*1000 erstellt (HA13)
	public static Rechteck generiereZufallsRechteck(){
		Rechteck rechteck = new Rechteck();
		rechteck.setX((int)(Math.random()*1200));
		rechteck.setY((int)(Math.random()*1000));
		rechteck.setBreite((int)(Math.random()*(1200-rechteck.getX())));
		rechteck.setHoehe((int)(Math.random()*(1000-rechteck.getY())));
		return rechteck;
	}
	
	// Getter und setter (HA2)
	public int getX() {
		return punkt.getX();
	}

	public void setX(int x) {
		this.punkt.setX(x);;
	}

	public int getY() {
		return punkt.getY();
	}

	public void setY(int y) {
		this.punkt.setY(y);;
	}

	public int getBreite() {
		return breite;
	}

	public void setBreite(int breite) {
		// absoluter Wert (HA8)
		this.breite = Math.abs(breite);
	}

	public int getHoehe() {
		return hoehe;
	}

	public void setHoehe(int hoehe) {
		// absoluter Wert (HA8)
		this.hoehe = Math.abs(hoehe);
	}

}
