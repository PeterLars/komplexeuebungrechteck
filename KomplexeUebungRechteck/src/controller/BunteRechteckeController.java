package controller;

import java.util.LinkedList;
import java.util.List;

import model.MySQLDatabase;
import model.Rechteck;
import view.Eingabemaske;

public class BunteRechteckeController {
	
	// Attribute (HA5)
	private List<Rechteck> rechtecke;
	private MySQLDatabase database;
	
	// Eine (noch) leere Main-Methode (HA5)
	public static void main(String[] args) {
		
	}
	
	// Eine Methode die Rechteck zur Liste hinzufügt (HA5)
	public boolean add(Rechteck rechteck) {
		database.rechteckEintragen(rechteck);
		return rechtecke.add(rechteck);
	}
	
	// Methode zum Löchen der Rechtecke (HA5)
	public boolean reset() {
		rechtecke.clear();
		return rechtecke.isEmpty();
	}
	
	// Gibt die den BunteRechteckeController mit all seinen Rechtecken in einem String aus (HA5)
	@Override
	public String toString() {
		return "BunteRechteckeController [rechtecke=" + rechtecke + "]";
	}
	
	// Konstruktoren (HA5)
	public BunteRechteckeController() {
		this.database = new MySQLDatabase();
		this.setRechtecke(this.database.getAlleRechtecke());
	}
	
	public BunteRechteckeController(List<Rechteck> rechtecke) {
		this.setRechtecke(rechtecke);
		this.database = new MySQLDatabase();
	}
	
	public void generiereZufallsRechtecke(int anzahl) {
		reset();
		for (int i = 0; i < anzahl; i++) {
			add(Rechteck.generiereZufallsRechteck());
		}
	}

	public void rechteckHinzufuegen() {
		new Eingabemaske(this);
	}
	
	
	// Getter und setter (HA5)
	public List<Rechteck> getRechtecke() {
		return rechtecke;
	}
	
	public void setRechtecke(List<Rechteck> rechtecke) {
		this.rechtecke = rechtecke;
	}
	
}
