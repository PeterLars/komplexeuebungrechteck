package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.BunteRechteckeController;
import model.Rechteck;

import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Eingabemaske extends JFrame {

	private JPanel contentPane;
	private JTextField txFY;
	private JTextField txFB;
	private JTextField txFL;
	private JTextField txFX;
	private BunteRechteckeController brc;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Eingabemaske frame = new Eingabemaske(new BunteRechteckeController());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Eingabemaske(BunteRechteckeController brc) {
		this.brc = brc;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(0, 1, 0, 2));

		JLabel lblX = new JLabel("X:");
		contentPane.add(lblX);

		txFX = new JTextField();
		contentPane.add(txFX);
		txFX.setColumns(10);

		JLabel lblY = new JLabel("Y:");
		contentPane.add(lblY);

		txFY = new JTextField();
		contentPane.add(txFY);
		txFY.setColumns(10);

		JLabel lblL = new JLabel("Länge:");
		contentPane.add(lblL);

		txFL = new JTextField();
		contentPane.add(txFL);
		txFL.setColumns(10);

		JLabel lblB = new JLabel("Breite:");
		contentPane.add(lblB);

		txFB = new JTextField();
		contentPane.add(txFB);
		txFB.setColumns(10);

		JButton btnSpeichern = new JButton("Speichern");
		btnSpeichern.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				speichern();
			}

			private void speichern() {
				Rechteck eck = new Rechteck(Integer.parseInt(txFX.getText()), Integer.parseInt(txFX.getText()),
						Integer.parseInt(txFX.getText()), Integer.parseInt(txFX.getText()));
				brc.add(eck);
				txFX.setText("");
				txFY.setText("");
				txFL.setText("");
				txFB.setText("");
			}
		});
		contentPane.add(btnSpeichern);
		setVisible(true);
	}

}
