package view;

import java.awt.Color;
import java.awt.Graphics;
import java.util.List;

import javax.swing.JPanel;

import controller.BunteRechteckeController;
import model.Rechteck;

@SuppressWarnings("serial")
public class Zeichenflaeche extends JPanel {
	
	// Attribute (HA6)
	private BunteRechteckeController myBunteRechteckeController;
	
	// Konstruktoren (HA6)
	public Zeichenflaeche(BunteRechteckeController bunteRechteckeController) {
		super();
		this.myBunteRechteckeController = bunteRechteckeController;
	}
	
	// Eine Methode zum Zeichnen aller Rechtecke der BunteRechteckeController Klasse (HA6)
	@Override
	public void paintComponent(Graphics g) {
		List<Rechteck> rechtecke = myBunteRechteckeController.getRechtecke();
		g.setColor(Color.BLACK);
		for (int i = 0; i < rechtecke.size(); i++) {
			g.drawRect(rechtecke.get(i).getX(), rechtecke.get(i).getY(), rechtecke.get(i).getBreite(), rechtecke.get(i).getHoehe());
		}
	}
	
	
}
